/*
    process data from config file

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iterator>
#include <locale>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace tyfir {

template <typename T, size_t N>
std::istream& operator>>(std::istream& in, std::array<T, N>& arr)
{
   in.ignore(1, '[');
   std::copy_n(std::istream_iterator<T>(in), N, std::begin(arr));
   return in;
}

template <typename T, size_t N>
std::ostream& operator<<(std::ostream& out, std::array<T, N>& arr)
{
   out << "[";

   if (N > 0)
   {
      out << arr[0];
   }

   for (int n = 1; n < N; ++n)
   {
      out << " " << arr[n];
   }

   out << "]";

   return out;
}

class Config
{
 public:
   using key_t   = std::string;
   using value_t = std::string;

   Config(const std::string& filename)
   {
      std::ifstream file(filename);

      if (!file)
      {
         throw std::ifstream::failure("Error opening \"" + filename + "\"!");
      }

      std::string line;

      while (std::getline(file, line))
      {
         auto stringvec = split(line);

         // add pair to map
         if (stringvec.size() == 2)
         {
            data_[stringvec[0]] = stringvec[1];
         }
         // throw error if only one or more than two '='-separated terms in line
         else if (stringvec.size() > 0)
         {
            throw std::invalid_argument(
                "Parameter File \"" + filename + "\":\n\tError in line:   \"" + line +
                "\"\n\tEach line must consist of a key-value pair, separated by an '=', e.g., 'KEY = VALUE'");
         }
      }
   }

   friend std::ostream& operator<<(std::ostream& out, const Config& config);

   template <class T>
   T get(const key_t& key) const
   {
      return get_<T>(key);
   }

   template <class T>
   T get(const key_t& key, const T deflt) const
   {
      try
      {
         return get_<T>(key);
      } catch (std::invalid_argument& e)
      {
         return deflt;
      }
   }

   bool exists(const key_t& key) const { return data_.count(key); }

 private:
   template <class T>
   T get_(const key_t& key) const
   {
      T value;

      auto result = data_.find(key);

      if (result == data_.end())
      {
         throw std::invalid_argument("Config::get(): Key \"" + key + "\" does not exist!");
      }

      std::istringstream valstr(result->second);

      if (!(valstr >> value))
      {
         throw std::runtime_error("Config::get(): Value \"" + result->second + "\" can not be converted to " + typeid(T).name() +
                                  "!");
      }

      return value;
   }

   static inline void trim(std::string& s)
   {
      auto whitespace = [](unsigned char ch) { return !std::isspace(ch); };
      // trim left
      s.erase(s.begin(), std::find_if(s.begin(), s.end(), whitespace));
      // trim right
      s.erase(std::find_if(s.rbegin(), s.rend(), whitespace).base(), s.end());
   }

   static inline std::vector<std::string> split(const std::string& line)
   {
      std::vector<std::string> result;
      std::string              item;

      if (line.empty())
         return result;

      // remove comments
      std::stringstream stream(line);

      while (std::getline(stream, item, '#'))
      {
         result.push_back(item);
      }

      std::string noComments = result[0];
      result.clear();

      // split in key-value pair
      stream = std::stringstream(noComments);

      while (std::getline(stream, item, '='))
      {
         trim(item);

         if (item.size() > 0)
            result.push_back(item);
      }

      return result;
   }

   std::map<key_t, value_t> data_;
};

std::ostream& operator<<(std::ostream& out, const Config& config)
{
   out << "{\n";
   for (auto& val : config.data_)
   {
      out << "  " << val.first << " = " << val.second << "\n";
   }
   out << "}\n";

   return out;
}

} // namespace tyfir