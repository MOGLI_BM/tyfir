/*
    process data from input file

    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <array>

namespace tyfir{

// read vectorial data from a file
template<class T>
std::vector<std::vector<T>> read_vectors(const std::string& filename)
{
    std::vector<std::vector<T>>     result;
    std::ifstream                   file(filename);
    std::string                     line;

    if (!file)
    {
        throw std::ifstream::failure(
                    "Error opening \"" + filename + "\"!");
    }

    while (std::getline(file, line))
    {
        std::istringstream stream(line);
        std::vector<T> vec;
        T el;

        while (stream >> el)
        {
            vec.push_back(el);
        }

        result.push_back(vec);
    }

    return result;
}

// read scalar data from a file with one entry per line
template<class T>
std::vector<T> read_scalars(const std::string& filename)
{
    auto intermediate = read_vectors<T>(filename);
    size_t n = intermediate.size();
    std::vector<T> result(n);

    for (size_t i = 0; i < n; ++i)
    {
        result[i] = intermediate[i][0];
    }

    return result;
}

} // namespace tyfir
